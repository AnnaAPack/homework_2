import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

/**
 * Created by Bravo on 13.04.2017.
 */
public class homework_2 {

    public static void  main(String[] args) {
        String BASE_URL_ADMINPANEL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
        String login = "webinar.test@gmail.com";
        String pswrd = "Xcg7299bnSmMuRLp9ITw";

        String page_title;
        String page_title_refreshed;
        String page_header;
        String page_header_refreshed;
        String menu_item_name;

        System.getProperty("driver.chrome");
        String property = System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);
        WebDriver driver = new ChromeDriver();


        driver.get(BASE_URL_ADMINPANEL);
        login(driver, login, pswrd);

        List<WebElement> menu_items = driver.findElements(By.className("maintab"));

        int n = menu_items.size();
        System.out.println("Menu items qty = " + n);

        Boolean flag = true;
        for (int i=0; i<n; i++)
        {
            try
            {
                menu_items = driver.findElements(By.className("maintab"));
                sleep(1000);
                menu_item_name = menu_items.get(i).getText();
                menu_items.get(i).click();
            }
            catch (IndexOutOfBoundsException e) //on clicking the "Каталог" menu item all locators are changed => there are no elements of the "maintab" class => menu_items.size()=0
            {
                menu_items = driver.findElements(By.className("link-levelone "));
                sleep(1000);
                menu_item_name = menu_items.get(i).getText();
                menu_items.get(i).click();
            }

            page_header = getHeader(driver);
            page_title = driver.getTitle();
            sleep(1000);

            driver.navigate().refresh();
            page_header_refreshed = getHeader(driver);
            page_title_refreshed = driver.getTitle();

            if ((page_header.equals(page_header_refreshed)) & (page_header.equals(page_header_refreshed)))
            {
                System.out.println("The '" +menu_item_name+ "' menu item is available");
                System.out.println("      (Headers: "+page_header+" = "+page_header_refreshed+")");
                System.out.println("      (Titles: "+page_title+" = "+page_title_refreshed+")");
            }
            else
            {
                System.out.println("The '" + menu_item_name + "' menu item isn't available");
                System.out.println("      (Headers: "+page_header+" vs "+page_header_refreshed+")");
                System.out.println("      (Titles: "+page_title+" vs "+page_title_refreshed+")");;
                flag = false;
            }
        }

        if (flag)
        {
            System.out.println("All main menu items are available");
        }
        else
            System.out.println("Main menu items test is failed");


        logout (driver);
        driver.quit();
    }


    //logs a user in using login_/pswrd_
    public static void login (WebDriver driver_, String login_, String pswrd_) {

        WebElement email = driver_.findElement(By.id("email"));
        email.sendKeys(login_);

        WebElement password = driver_.findElement(By.id("passwd"));
        password.sendKeys(pswrd_);

        driver_.findElement(By.name("submitLogin")).click();
        sleep(1000);

        System.out.println("User is logged in");
    }


    //logs a user out
    public static void logout (WebDriver driver_) {
        driver_.findElement(By.className("employee_avatar_small")).click();
        driver_.findElement(By.id("header_logout")).click();
        System.out.println("User is logged out");

    }

    //returns page title
    public static String getHeader(WebDriver driver) {
        String result="";

        try {
            result =  driver.findElement(By.className("page-title")).getText();
        }
        catch (NoSuchElementException e) {
            result = driver.findElement(By.tagName("h2")).getText();}

        return result;
    }

    public static void  sleep(long ms) {
        try {
            Thread.sleep(ms);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
